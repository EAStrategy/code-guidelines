# JavaScript

## jshint
All javascript is expected to pass 100% with all options present in .jshintrc.

For client side javascript files, the only option that is allowed to be overridden in the `/*jshint*/` is assume browser.

    /*jshint browser: true*/

For node server side javscript files, a few more options are allowed to be
overridden in the `/*jshint */` statment such as:

    /*jslint node: true, _nomen: true */

Minified javascript files should have `.min` in the filename and do not need to
be tested by jshint.  Minified files should be javascript that is out of your
control to modify.


## jQuery Selectors

### IDs 
IDs should be selected using the native `document.getElementById()` instead of using jQuery shorthand. This [greatly improves performance][getelementbyid].

    jQuery(document.getElementById('js-idName'));


### Classes
Due to [browser incompatability with IE8][getelementsbyclassname], jQuery shorthand should be used to select class names (until we stop supporting IE8, that is).

    jQuery('.js-className');


## Global variables
Global variables should be kept to an absolute minimum.  Exceptions are
when creating a namespace for your own use.  This should be declared in a
single `var` statement at the top of the file.  All global variable should be 
invoked via dot notation off of the window object.


## Variable declaration
All variables should be declared at the top of the function in a single `var`
statement.


## Semicolon
All JavaScript statements should end in a semicolon.


## Comma
Commas should be used as a separator, or an operator in the initialization and
incrementation parts of a `for` statement.

Commas should be used sequentionally (`,,`) in an array literal.

A comma should not be used as the last element of an array or object literal.


## Required blocks
`if`, `while`, `do`, and `for` statments must be made with blocks.  That means
the { braces } are not optional


## For in statements
All `for in` statements are required to have the body wrapped in an `if`
statement for filtering, for example:

    for (name in object) {
        if (object.hasOwnProperty(name)) {
            ...
        }
    }


## Switch statments
The statement before the next `case` or `default` in a `switch` statment should
be either `break`, `return`, or `throw`.  A switch statement should be formatted
like this:

    switch (variable) {
    case 'foo':
        statement();
        break;

    case 'bar':
        statement();
        break;

    default:
        statement();
    }


## With statements
Do not use `with` statements, use a `var` instead. Refer to [this post][1] if
you would like more infomration on why this is not a good idea.


## Assignment statements
The assignment statement, `=`, is not allowed within an `if`, `for`, `while`,
or `do` conditional statement.


## Equality operators
Do not use the equality operators `==` and `!=`.  These will do type coercion.
Use the true equality operators instead, `===` and `!==`.


## Conditional boolean
When evaluating truthy or falsy values, use the short form.  Use `(foo)` instead
of `(foo != 0)` and `(!foo)` instead of `(foo == 0)`


## eval
Do not use eval, its evil.


[1]: http://yuiblog.com/blog/2006/04/11/with-statement-considered-harmful/
[getelementbyid]: http://jsperf.com/jquery-sharp-vs-getelementbyid
[getelementsbyclassname]: http://caniuse.com/getelementsbyclassname
