# Freemarker
- Don't use Freemarker for the Expansion project.

- All Functions should have one return statement.

- Define all variables with defaults as high as possible. At top of each .ftl
  file, at top of macros, at top of functions.

- Do not define variables within an `<#if>` block. Sure, set a variable within
  an `<#if>`, but define it with a default value above the conditional!

- Insure all variables have defaults

- Insure all multiple node references are wrapped in `()` with defaults.
  Example: `(content.something)!"default value"`

- Do not omit the `""` when using the default operator `!` when you want the
  default to be an empty string

- Do not use `??` as a shortcut for `?has_content`, they are not the same and
  may not behave as you expect.

- Do not use assign tags in macros or functions, always use local tags

- Use switch statements over if elseif when possible

- Put all strings in the json config, not hard coded in the ftl files

- Strings should be in strings.en_US in the json config.

- Do not call `Http.get()` directly, call `httpGetWithErrorHandling()`. This is
  part of the utility-lib.ftl.

- Do NOT rely on global assign variables inside of macros and functions. If you
  need to use something in a macro or function, you must pass it in. The
  language will let you use a global variable, but this is very bad.

- Avoid use of `<#attempt><#recover></#attempt>` blocks. These tend to clog the
  logs unnecessarily. An example of refactoring an attempt/recoverblock:
        
        BAD
        <#attempt>
            <#assign foo = bar.value>
        <#recover>
            <#assign foo = defaultVal>
        </#attempt>

        GOOD
        <#assign foo = (bar.value)!defaultVal>

- DO NOT build sequences or hashes via a loop as you would arrays. According to
  the Freemarker manual concatenating sequences and hashes always results in a
  new object which is slower to read, and therefore "concatenation is not to be
  used for many repeated concatenations, like for appending items to a sequence
  inside a loop." Instead, try to write your looping code to build the intended
  HTML directly. If absolutely necessary, pick a special delimiter you can
  guarantee will not exist in your generated HTML and use it to build a string-
  representation of the sequence you wish to build; then you can later pick
  items out of this string as you would a sequence. For example:

        This is a bit hacky, though, so do this as a last resort.
        Example of string-as-array code in Freemarker
        <#function addStateHtml state arrayStr>
            <#--
                This function generates HTML for a given state and stores it
                in arrayStr in a way that lets us get it back later as though
                arrayStr were an array of HTML blocks.
            -->
         
            <#local markerBeginHtml = "<!-- STATE " + state + " HTML -->">
            <#local markerEndHtml = "<!-- /STATE " + state + " HTML -->">
         
            <#local stateHtml = "<h1>This is the HTML for state ${state}.</h1>">
         
            <#local retval = markerBeginHtml + arrayStr + markerEndHtml>
            <#return retval>
        </#function>
         
        <#function getStateHtml state arrayStr>
            <#--
                This function returns only the HTML we've created for the given state
                found in the arrayStr string.
            -->
         
            <#local markerBeginHtml = "<!-- STATE "+ state + " HTML -->">
            <#local markerEndHtml   = "<!-- /STATE "+ state + " HTML -->">
         
            <#local indexStart = arrayStr?index_of(markerBeginHtml)>
            <#if indexStart < 0>
                <#return "">
            </#if>
         
            <#local indexEnd = arrayStr?index_of(markerEndHtml, indexStart)>
            <#if indexEnd < 0>
                <#return "">
            </#if>
         
            <#local retval = arrayStr?substring(indexStart, indexEnd)>
            <#return retval>
        </#function>

- Try to avoid else conditions on assigning variables

        GOOD
        <#assign tableRowsPrimaries = tableRowsPrimaries + generateTableRow(race, firstPrimary)>
        <#assign firstPrimary = false>
         
        <#if ((race.primarytype) == "caucus")>
            <#assign tableRowsCaucuses = tableRowsCaucuses + generateTableRow(race, firstCaucus)>
            <#assign firstCaucus = false>
        </#if>
         
         
        BAD
        <#if ((race.primarytype) == "caucus")>
            <#assign tableRowsCaucuses = tableRowsCaucuses + generateTableRow(race, firstCaucus)>
            <#assign firstCaucus = false>
        <#else>
            <#assign tableRowsPrimaries = tableRowsPrimaries + generateTableRow(race, firstPrimary)>
            <#assign firstPrimary = false>
        </#if>

- Always wrap conditionals that use greater than or less than in parens

        GOOD
        <#if (a > b)>
         
        BAD
        <#if a > b>

- Define macros in separate files. If you do this, you can namespace them (which
  is a very good thing to do):

        <#-- Importing a macro file into a namespace called "ns" -->
        <#import "macrofile.ftl" as ns>
         
        <#-- Calling a macro defined in that file -->
        <@ns.macroName>

- If you need to use a global variable created outside your macro with the
  `<#assign>` directive, you can refer to it inside your macro using the .main
  namespace:

        <#assign globalHorseName = "Paul Revere">
        <#macro rideAcrossTheLand>
            I had a little horsey named ${.main.globalHorseName}.
        </#macro>

- Do not use HTML comments in FTL files, use FTL comments
