# Markdown
Markdown is the language we use to write lots of documentation that range from
simple READMEs in git repositories to complex documentation.  The standard for
this is defined at [Daring Fireball][0].


- All markdown files should end in .md

- All markdown files should be space delimited

- Maximum line length should not go over 80 columns, unless it is part of a
  code block.

- All links should NOT be inline, they should be labelled at the bottom of the
  document.  There should be three blank lines separating the content of the
  document and the label table.

        BAD - [http://cnn.com](http://cnn.com)

        GOOD - [http://cnn.com][0]
        ... at the bottom of the file ...



        [0]: http://cnn.com

- Headings should use the single line format, not the double line format.

        # This is a good H1 heading

        This is a bad H1 heading
        ========================

- Headings should be preceeded by at least two blank lines and should be
  immediatley followed if the next content is not a code block, which requires
  a leading blank line.

        This is the end of the previous paragraph.


        ## This is the next heading
        This is the beginning of the next paragraph.

- Bullet lists should use a - not a * and have a blank line separating them.

        - GOOD

        - GOOD

        * BAD
        * BAD

- Lines that wrap in a bullet list should be indented equal to the beginning of
  the first line of the bullet.

        - GOOD - This is a wrapping line
          that is indented properly

        - BAD - This is a wrapping line
        that is not indended properly

- Code blocks can be done two different ways.  Markdown specifies they should be
  indented 4 spaces or 8 spaces if they need to be indented for a bullet point.

- Code blocks on bitbucket allow a differnet syntax for code highlighting.  Be
  aware that this formating is not suported outside of Bitbucket.  You start the
  block with three ticks followed by a language, then close the block with three
  ticks

        ```css
        .box {
            -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
               -moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
                    box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
        }
        ```



[0]: http://daringfireball.net/projects/markdown/syntax
