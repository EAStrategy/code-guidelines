# SEO
## General

- All pages should support [schema.org][0] where applicable.
- All pages, cards, components, etc. should utilize [ARIA][1] attributes when applicable.


## URLs

- All URLs should be lowercase

- All topic pages should live within a related and relevant section.  Example:
  A topic page on "Haiti Earthquake" should live in www.cnn.com/world/ and have
  a URL that reads www.cnn.com/world/haiti-earthquake/


## HTML Tags

- Canonical tags should be absolute, not relative

        <link rel="canonical" href="http://www.cnn.com/world/haiti-earthquake/" />

- Title tags should be programmatic and read "{TOPIC} - Videos, Pictures, and
  News - CNN.com"

        <title>Haiti Earthquake - Videos, Pictures, and News - CNN.com</title>

- Meta description tag would be programmatic and read "News on {TOPIC} including
  breaking news and archival information from CNN.com."

        <meta name="description" content="News on Haiti Earthquake including breaking news and archival information from CNN.com." />  

- There should be one, and only one, H1 tag on the page that lists the official
  name of the page.

- H2 tags should be used for each card title.

- Meta keywords tag should include the following items:
  - Name of page.
  - If more than one word, each word should also be separated by commas
  - Site section
  - If CAP can provide additional "related" keyterms, they should also be added
    to the tag, separated by commas. This includes misspellings.


## Videos

- Videos should be available within cards.

- Video caption/link needs to be crawable when not lazyloaded.


## Images / Galleries

- Galleries should be available within cards.

- Gallery name/link should be crawable when not lazyloaded.

- If there is an official image for each topic page, ensure its filename
  includes the keyterm of the topic page.

        <img src="http://i2.cdn.turner.com/assets/haiti-earthquake-main.jpg" width="620" height="348" alt="Haiti Earthquake" />

- If there is an official image for each topic page, ensure it has associated
  alt text that includes the keywords of the page.

        <img src="http://i2.cdn.turner.com/assets/haiti-earthquake-main.jpg" width="620" height="348" alt="Haiti Earthquake" />


## Internal Linking

- Topic cards must be crawlable by search engine spiders when not lazyloaded.




[0]: http://schema.org/docs/documents.html
[1]: http://dev.w3.org/html5/markup/aria/aria.html
