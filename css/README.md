# CSS

Before reading this, you should have a general understanding for
**specificity**, the [SCSS syntax][10], and [KSS documentation][11].

----

**Table of Contents**

1. Sass
1. Reset
1. Whitespace
1. Formatting
1. Object Hierarchy
1. Nest Media Queries
1. Never Use IDs
1. Comments
1. File Structure
1. Targeting IE

----


## Sass

Use the `.scss` syntax, not `.sass`.

- Do not mix small amounts of Sass in with plain CSS. If you're converting a
  file from CSS to Sass, go ahead and convert the entire file.

- As a rule of thumb, don't nest further than 3 levels deep. If you find
  yourself going further, think about reorganizing your rules (either the
  specificity needed, or the layout of the nesting).

- Utilize [partials][13] to break up your project into logical chunks.

- List `@extend`s first, regular styles section, `@include`s, then nested
selectors (via [css-tricks][17])


## Reset
- We're using [Normalize.css][9]

    - [Normalize.css][8] makes browsers render all elements more consistently
      and in line with modern standards. It precisely targets only the styles
      that need normalizing.

- You shouldn't have to worry about including this, since it's global. It is
  included in the [CNN Pattern Library][12].


## Whitespace
- Use tabs with a tab size of 4 spaces.

- Never mix tabs and spaces

- Delete trailing whitespace


## Formatting
- Put a single space before `{` in rule declarations.

- Put a single space after `:` in property declarations.

- Always include the trailing semi-colon in a ruleset.

- Use lowercase hex values, e.g. `#eebc9a`.

- Use shorthand hex values when possible, e.g. `#000`.

- Alphabetize CSS declarations, e.g. `background` before `color`

- Separate each rule by a single blank line.

- Separate each comma-separated value with a space, e.g. `rgba(0, 0, 0, 0.5)`.

- Use single quotes, e.g. `background: url('/img/pattern.png')`.

- Use quotes in selectors with attribute values, e.g. `input[type='checkbox']`.

- Use shorthand properties when possible, e.g. `padding: 10px 20px`.

- Avoid specifying units for values of zero, e.g. `margin: 0 10px 0 25px`.

Comma-separated values should be broken up onto multiple lines and indented one
level further than the property.

```scss
.something {
  background:
    url('/img/gradient.png') left top repeat-x,
    url('/img/noise.png') left top repeat;
  box-shadow:
    0 0 10px #222,
    0 0 10px #ccc inset;
}
```


### Vendor Prefix Alignment

```scss
.box {
  -webkit-box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
     -moz-box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
          box-shadow: 0 0 10px rgba(0, 0, 0, 0.5);
}
```


## Nest Media Queries

Mobile styles should be applied directly on an element. Tablet and desktop
Media Queries should be nested in the form of the ```breakpoint``` mixin.

```scss
.parent {
    padding: 5px;
    @include breakpoint($desktop) {
        padding: 10px;
    }

    .child {
        float: left;
        @include breakpoint($desktop) {
            float: none;
        }
    }
}
```

If this produces a long SASS file, break out the logical pieces into `@include`d
[partials][13]. Do not break out Media Queries into partials.


## NEVER use IDs & JS Specific Hooks
**Never use a CSS styling class as a JavaScript hook.** If you need to bind JS
to a class / ID, namespace it with either `.js-` or `#js-`

Attaching JS behavior to a styling class means that we can never have one
without the other.

_NEVER_ use IDs in CSS.

They can be used in your markup for JS and fragment identifiers but **use only
classes for styling.** We don’t want to see a single ID in any stylesheets.

Classes come with the benefit of being reusable (even if we don’t want to, we
can) and they have a nice, low specificity. Specificity is one of the quickest
ways to run into difficulties in projects and keeping it low at all times is
imperative. An ID is [255 times more specific][7] than a class, so never ever
use them in CSS. Ever.


## Understand the Difference Between Block vs. Inline Elements
Block elements are elements that naturally clear each line after they’re
declared, spanning the whole width of the available space. Inline elements take
only as much space as they need, and don’t force a new line after they’re used.

Here are the lists of elements that are either inline or block:
`span, a, strong, em, img, br, input, abbr, acronym`

And the block elements:
`div, h1...h6, p, ul, li, table, blockquote, pre, form`


## Comments
- Use `//` comment blocks -- instead of `/* ... */`

We are currently switching over to living styleguides - this means you will
write comments
for your stylesheets very similar to javadoc.

Example: [KSS Living Styleguides.][8]

Don't be afraid to comment your code. We strip out all comments for production,
so have at it.

- Add markup to better explain blocks of code.

- Link off to articles.

- Link to live working examples ([jsfiddle][6]).


## File Organization
_NOTE: This will be expanded upon going forward. A lot of the global styles will
be moved into the [CNN Pattern Library][12]_

One of the great things about Sass is that it allows us to split our files up
into logical chunks. Now we can use our `.scss` files as manifests - picking and
choosing what we want to import.

```
├── _colors.scss
├── _footer.scss
├── _header.scss
├── _homepage.scss
├── _mixins.scss
├── _navigation.scss
├── _normalize.scss
└── page.scss
```

Inside of `page.scss` you would `@import` all the [Sass partials][13] needed.

```
@import 'normalize';
@import 'mixins';
@import 'colors';
@import 'homepage';
@import 'header';
@import 'navigation';
@import 'footer';
```


## Targeting Different Versions of IE
We're conditionally setting classes on the `html` element, so you can easily
target the different versions of Internet Explorer.

```html
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
```
To target IE 8, you would simply use `.lt-ie9`.

```scss
.lt-ie9 .header {
	// do stuff
}
```


## Object Hierarchy

We've implemented a class naming system for our SCSS with roots in [OOCSS][15], drawing largely from the [BEM][14] approach.

### Module Objects

A `Module`, the molecule in our hierarchy, is a reusable grouping of associated Elements. This is the rough equivalent of a `Block` in BEM. A Module will provide the namespace to its enclosed elements.

There are specific kinds of Modules, as we will see later. They receive a short prefix to indicate their classification. If a Module doesn't fit into a specific category, it gets the `m-` prefix, such as `m-header`.

```html
<header class="m-header">
    < ... />
</header>
```

**NOT**

```html
<header>
    < ... />
</header>
```

### Element Objects

An `Element` is the atom in our hierarchy. It is simply an HTML element. It has the same name and meaning in the BEM method. It must be explicitly tied to a Module through a namespace, and cannot be used on its own. Elements are prefixed with the full class name of its parent Module, followed by `__` (two underscores) and then a unique name. For example, `m-header__logo`.

```html
<header class="m-header">
    <a class="m-header__logo" href="/"> ... </a> 
</header>
```

**NOT**

```html
<header class="m-header">
    <a href="/"> ... </a>
</header>
```

Since Elements are namespaced, their class names should be so specific that they are targeted using only the class, without parent selectors or other qualifiers.

```scss
.m-header__logo {
    background-image: url('...');
}
```

**NOT**

```scss
.m-header {
    a.m-header__logo {
        background-image: url('...');
    }
}
```

### Component Objects

If an Element can be shared across multiple Modules, it no longer gets tied to a Module via a namespace. Elements that are not owned by a Module are called `Components`. They should be reusable so that they can be dropped in any Module and added to the Pattern Library. Components gets a `c-` prefix. For example, `c-foo`.

```html
<header class="m-header">
    <div class="c-logo"> ... </div>
</header>

<footer class="m-footer">
    <div class="c-logo"> ... </div>
</footer>
```

**NOT**

```html
<header class="m-header">
    <div class="m-header__logo"> ... </div>
</header>

<header class="m-footer">
    <div class="m-footer__logo"> ... </div>
</header>
```

Note that Modules can be used inside of other Modules, so the difference between Components and Modules comes down to size and scope. If the header and footer modules share identical markup for the site navigation, the navigation should be a Module instead of a Component because there are multiple Elements involved.

```html
<header class="m-header">
    <a class="c-logo" href="#"> ... </a>

    <nav class="m-nav">
        <ul class="m-nav__list">
            <li class="m-nav__item"> ... </li>
            ...
        </ul>
    </nav>
</header>

<footer class="m-footer">
    <nav class="m-nav">
        <ul class="m-nav__list">
            <li class="m-nav__item"> ... </li>
            ...
        </ul>
    </nav>
</footer>
```


#### Deep Nesting

Elements can contain children of their own.

```html
<div class="m-foo">
    <div class="m-foo__bar">
        <div class="m-foo__bar__baz">...</div>
    </div>
</div>
```

Be careful not to nest too deep. If it applies in a modular sense, consider resetting the namespace to avoid extremely long selectors.

```html
<div class="m-foo">
    <div class="m-bar">
        <div class="m-bar__baz">...</div>
    </div>
</div>
```

### Layout Objects

Non-semantic structural elements that are not associated with any other object get a special prefix `l-`. They do not fit neatly into the object hierarchy, so they can be placed anywhere.

```html
<div class="l-container">
    <div class="m-foo">
        < ... />
    </div>
</div>
```

Layout objects can be used in conjunction with other objects as Modifiers (more on Modifiers in a moment).

```html
<div class="m-foo l-container">
    < ... />
</div>
```

## Modifiers

### Namespaced Object Modifiers

Any Module or Element can have variations, or `modifications`. Consistent with BEM style, `Modifiers` are prefixed with the full class name of the object being modified, followed by `--` (two dashes) and then a unique name. For example, `m-foo__bar--baz`.

```html
<div class="m-foo">
    <div class="m-foo__bar m-foo__bar--baz">
        < ... />
    </div>
</div>
```

#### Avoiding overrides

One of the basic principles of our SCSS styleguide is avoiding overrides. [Rulesets should only inherit, never undo.][18] If a particular CSS property is set multiple ways with multiple Modifiers, that property should not be applied to the object's base, otherwise the modifiers would have to undo/override inherited styles.

On this example, `font-size` doesn't get a default, meaning that property is either designed to be inherited, or that it *requires* a Modifier. Either way, it should be commented in code.

```scss
.c-headline {
    // requires Modifier for font-size
    font-family: $sans-serif;
}

.c-headline--large {
    font-size: 48px;
}

.c-headline--omfg {
    font-size: 96px;
}
```

If there are many Modifiers and they all assume the same condition, except for a single outlier, an override may be permitted, but should be commented as such using `!override` (note similarity to `!important`). Avoid this whenever possible.

```scss
.c-headline {
    font-family: $sans-serif;
}

.c-headline--large {
    font-size: 48px;
}

.c-headline--omfg {
    font-size: 96px;
}

.c-headline--small {
    font-family: $serif; // !override
    font-size: 18px;
}
```

If a Modifier varies widely enough, or if a default property on a base object needs to be modified, consider categorizing it as a different object entirely instead of trying to force commonality through a Modifier. There is still an opportunity for code reuse with an `@extend` class.

```scss
%c-foo-base {
    background-image: url('...');
    background-postion: 50% 50%;
    background-size: cover;
}

.c-foo {
    @extend %c-foo-base;
    border: 1px solid;
    font-size: 24px;
}

.c-foobar {
    @extend %c-foo-base;
    border: 5px dotted;
    font-size: 48px;
}
```

**NOT**

```scss
.c-foo {
    background-image: url('...');
    background-postion: 50% 50%;
    background-size: cover;
    border: 1px solid;
    font-size: 24px;
}

.c-foo--bar {
    border: 5px dotted; // !override
    font-size: 48px; // !override
}
```

#### Layout and Spacing Rules

Margins, floats, width, display, and arguably padding variances do not always require a Modifier. If spacing depends on the context a reusable object (such as a Component) is placed in, remove the layout rules from the base styles and apply them by wrapping in a parent selector.

```scss
.c-logo {
    background-image: url('logo.png');
    background-size: cover;
}

.m-header {
    .c-logo {
        float: left;
        margin-bottom: $gutter-width;
        margin-right: $gutter-width;
        width: 50%;
    }
}

.m-footer {
    .c-logo {
        display: inline-block;
        width: 25%;
    }
}
```

### OOCSS Modifiers

Beyond namespaced object Modifiers, some Modifiers can be applied generically in OOCSS style.

#### Apply Modifiers

Look for repeatable design patterns and abstract them out as silent or explicit `apply-` classes.

Implicit:

```scss
%apply-vertical-list {
    list-style: none;
}

%apply-vertical-list__item {
    display: inline-block;
}

.m-tweets {
    @extend %apply-vertical-list;
}

.m-tweets__item {
    @extend %apply-vertical-list__item;
}
```

```html
<ul class="m-tweets">
    <li class="m-tweets__item">
        ...
    </li>
</ul>
```

Explicit:

```scss
.apply-foo {
   list-style: none; 
}

.apply-foo__bar {
    display: inline;
}
```

```html
<ul class="m-tweets apply-foo">
    <li class="m-tweets__item apply-foo__bar">
        ...
    </li>
</ul>
```

Both are fine, but `@extend` classes can cause CSS bloat.

#### Themes

Colors (including border-color and background-color) should almost never be applied directly to a base object. They should instead be applied using Themes. CNN.com is specifically designed so that the same objects can easily appear in different color contexts, often set on a parent, cascading down. This means most Modules need to account for possible Themes using SASS's special `&` parent selector. In cases where Themes are set directly on an element, the `&` moves before the class name, chaining the two classes together.

There are currently three Themes: `tan-light`, `blue-dark`, and `gray-dark`, each using a different color pallet in the Pattern Library. Form validation would also be a good use case for themes.

Themes drop the `apply-` prefix in favor of the special `t-` prefix.

```scss
.m-foo {
    border: 1px solid;
    font-size: 14px;

    .t-tan-light & {
        background-color: $tan-6;
        border-color: $tan-9;
    }

    .t-blue-dark & {
        background-color: $blue-3;
        border-color: $blue-1;
    }

    .t-gray-dark & {
        background-color: $gray-3;
        border-color: $gray-1;
    }
}
```

**NOT**

```scss
.m-foo {
    background-color: $blue-3;
    border: 1px solid $blue-1;
    font-size: 14px;
}

.t-tan-light {
    .m-foo {
        background-color: $tan-6; // !override
        border: 1px solid $tan-9; // !override
    }
}

.t-gray-dark {
    .m-foo {
        background-color: $gray-3; // !override
        border: 1px solid $gray-1; // !override
    }
}
```

There are instances where colors need to be consistent regardless of theme. This usually only applies for branding purposes, or if a single color works for all possible themes.

```scss
.c-logo {
    background-color: $red-2;
}
```

## Card-Container-Zone System

As we noted briefly before, there are different classifications for a Module. These are based on very specific business rules, collectively the `Card-Container-Zone (CCZ) System`. This is a prescribed visual hierarchy.

In short, `Cards` can be combined into `Containers`, which can be placed inside of `Zones`, which are combined to make `Pages`. Each of these levels in the hierarchy must be accounted for in the markup. A Card cannot be a direct descendent of a Zone, for example. The Container level must always separate them.

Since Cards, Containers, Zones, and Pages are all just specific types of Modules, their specific internals can be namespaced, such as `zn__title`.

### Cards

A `Card` is a summary Module that represents a single URL. It almost always deserves an `article` tag.

Like the other non-general Module types, Cards drop the `m-` prefix. Instead, they get a `cd-` type prefix:

* `cd-story`
* `cd-person`
* `cd-assignment`
* `cd-show`
* `cd-topic`
* `cd-moment`

Cards get special Modifiers, depending on size and orientation:

* `cd--extra-small`
* `cd--small`
* `cd--medium`
* `cd--large`
* `cd--horizontal`
* `cd--vertical`

```html
<article class="cd cd-story cd--large cd--vertical">
    < ... />
</article>
```

### Containers

`Containers` are collections of related Cards. The type Modifiers generally instruct how to assemble their internal Cards. They may have a title and CTA footer. Containers get a `cn-` type prefix.

```html
<div class="cn cn-list-simple">
    <h2 class="cn__title">The Latest</h2>
    <article class="cd cd-story">
        < ... />
    </article>
    <article class="cd cd-story">
        < ... />
    </article>
</div>
```

### Zones

A `Zone` is a grouping of Containers. It should always receive a `section` tag. Themes are applied at the Zone-level and cascade down. Zones can have a title. Besides Containers, Zones can also contain `Elements` directly, according to the Template Model. Zones get a `zn-` type prefix.

```html
<section class="zn zn-news t-blue-dark">
    <h2 class="zn__title">News</h2>
    <div class="cn cn-list-hierarchical">
        < ... />
    </div>
    <div class="cn cn-list-simple">
        < .. />
    </div>
</div>
```

The one special type of Zone is the `Staggered Zone`, which always has three columns.

```html
<section class="zn zn-staggered t-tan-light">
    <div class="zn__column">
        ...
    </div>
    <div class="zn__column">
        ...
    </div>
    <div class="zn__column">
        ...
    </div>
</section>
```

### Pages

`Pages` are templates containing Zones. Pages get a `pg-` type prefix.

There are three types of Pages.

* `pg-landing`
* `pg-leaf`
* `pg-list`

```html
<div class="pg pg-landing">
    <section class="zn zn-hero t-gray-dark">
        < ... />
    </section>
    <section class="zn zn-news t-tan-light">
        < ... />
    </section>
</div>
```



[0]: http://grab.by/gSGG "separate lines"
[1]: http://grab.by/gSGC "same line"
[2]: http://grab.by/gSEu
[3]: http://grab.by/gSEI
[4]: http://grab.by/gSF6
[5]: http://grab.by/gSIa
[6]: http://jsfiddle.net/
[7]: http://stackoverflow.com/questions/2809024/points-in-css-specificity/11934505#11934505
[8]: http://warpspire.com/kss/styleguides/
[9]: http://necolas.github.com/normalize.css/
[10]: http://sass-lang.com
[11]: https://github.com/kneath/kss
[12]: https://bitbucket.org/vgtf/cnn-pattern-library
[13]: http://sass-lang.com/docs/yardoc/file.SASS_REFERENCE.html#partials
[14]: http://bem.info/method/definitions/
[15]: http://oocss.org/
[16]: http://smacss.com/
[17]: http://css-tricks.com/sass-style-guide/
[18]: https://speakerdeck.com/csswizardry/big-css?slide=39
