# Code Guidelines
This package details all of the code guidelines that must be followed for
development at CNN.com.  Every commit must be done in a feature branch and
all feature branches must use pull requests to get merged into the parent
branch.  All pull request code reviews will require the standards set by these
code guidelines.

## Maintainers
- james.young@turner.com
- john.hayes@turner.com
- matt.crutchfield@turner.com

## Global
- All indentation should be spaces, not tabs. (Unless it is in a Makefile)

- Do not `return` from inside of a conditional

## [Markdown][markdown]

## [HTML][html]

## [CSS][css]

## [Dust][dust]

## [JavaScript][javascript]

## [Freemarker][freemarker]

## [PHP][php]

## [SEO][seo]


If you would like to contribute, see this [contributing document][contributing]


[markdown]: /eastrategy/code-guidelines/src/master/markdown/README.md
[html]: /eastrategy/code-guidelines/src/master/html/
[css]: /eastrategy/code-guidelines/src/master/css/
[dust]: /eastrategy/code-guidelines/src/master/dust/
[javascript]: /eastrategy/code-guidelines/master/javascript/
[freemarker]: /eastrategy/code-guidelines/master/freemarker/
[php]: /eastrategy/code-guidelines/src/master/php/
[contributing]: /eastrategy/code-guidelines/src/master/CONTRIBUTING/
[seo]: /eastrategy/code-guidelines/src/master/seo/
