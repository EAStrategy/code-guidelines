# Contributing
Contributions are welcome and encouraged.  Here is how to do that.

1. Fork the repository
2. Make your changes
3. Create a pull request to have your changes reviewed

## Things to be aware of
- These guidelines are here to insure that code is maintainable, scaleable,
  and most importantly readable.

- Contributions are for new guidelines.  If you want to to change a guideline
  that is already set, you will need a really, _really_ good reason to change
  it.

- Don't get your feelings hurt if we do not accept your pull request.  We are
  very picky and will not just accept everything that is contributed.

Thanks!
