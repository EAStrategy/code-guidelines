# Dust.js

Dust is a JavaScript templating engine designed to provide a clean separation
between presentation and logic without sacrificing ease of use.

## Before You Read

Writing a template is mostly writing HTML. Be sure you follow all our
[HTML code guidelines][0] when crafting a Dust template.

If you need any info on how to use Dust, visit the [LinkedIn Dust Tutorial][1].

## Partials

Always reference partial templates using the fully-qualified package/template
format, like so:

    {>"cnn-node-global/dust/header"/}

This is what allows server- and client-side modules to share sub-module code.

## Helpers

Dust allows us to create helpers which give the template more logical abilities
(as opposed to simply displaying data). As a rule, this is not something we
want to do.

**Do not create any new Dust Helpers** without sign-off from some-
one in an architectural or senior development role. It is just not often
necessary.

Use existing helpers whenever necessary.


[0]: https://bitbucket.org/vgtf/cnn-code-guidelines/src/26e46792f44d/html?at=master
[1]: https://github.com/linkedin/dustjs/wiki/Dust-Tutorial
