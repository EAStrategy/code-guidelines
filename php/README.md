# PHP

- Do NOT use sessions.

- Strings should be in single quotes, unless there is a variable in the string
  that needs to be parsed, then it is ok to use double quotes.

- Referring to a variable by-reference is slower than referring to a variable
  by-value (&$a vs. $a)

- Use BCMath for precision math.

- Use `<?php`, `<?` is deprecated.

- Check the log file, fix all errors and warnings.
