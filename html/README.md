# HTML

## `<!DOCTYPE>`
The HTML5 doctype should be used.

```html
 <!DOCTYPE html>
```

## `<html>`
This is how the html pages should start out:

```html
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
```


## `<head>`
### Modernizr
[Modernizr][1] should be utilized if appropriate.


## `<body>`
### Tag case
- All html tags should be in lower case.


### Closing tags
- All html tags should have closing tags.

- Single tag elements should be self closing.


### Nesting
- All nesting of tags should be properly nested.


### Attribute values
- All attributes should be quoted with double quotes


### ID names
- All  names should be properly spelled, descriptive, and unique.

- Do not use IDs as styling hooks - please refer to the [CSS Styleguide][2]
  for more information on this.

### Class names
- All class names should be properly spelled and descriptive.


### Indentation
- Indentation should be tabs with a tab size of 4.



[0]: https://github.com/Modernizr/Modernizr
[1]: https://bitbucket.org/vgtf/cnn-code-guidelines/src/master/css/README.md
